﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyReceiveDamage : MonoBehaviour
{
    public float health = 10f; //health value.
    public float damageTick = 5f; //damage taken per hit

    void OnTriggerEnter(Collider obj)
    {
        //for same damage per different weapon hit.
        if (obj.gameObject.tag == "Sword") //remember this is what the tag of the weapon was.
        {
            print("Collision detected.");
            health -= damageTick;
            CheckDeath();
        }
    }

    void CheckDeath()
    {
        if (health <= 0)
            Destroy(gameObject);
    }


}
