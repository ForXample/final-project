﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerReceiveDamage : MonoBehaviour
{
    public int currentHealth;
    public int maxHealth;
    public int slimeDamage;

    void Start()
    {
        currentHealth = maxHealth;    
    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Enemy") //remember this is what the tag of the weapon was.
        {
            print("Collision detected.");
            currentHealth -= slimeDamage;
            CheckDeath();
        }
    }

    void CheckDeath()
    {
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
            SceneManager.LoadScene(2);    //"Try again? / Exit."
        }
    }
}
